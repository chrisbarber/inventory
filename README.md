# inventory

Coding sample for an inventory tracking backend

## instructions

    pip install -r requirements.txt

Typical django setup (run migrations, create superuser, etc)

As admin under `admin/` create a "support team" and "normal" (read-only) groups with desired permissions; add one or more users in each group. Login under `site/` as any of these users or as admin.
