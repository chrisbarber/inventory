import re

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class CameraBrand(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


def positive_float(x):
    return x > 0.

class CameraModel(models.Model):
    name = models.CharField(max_length=200, unique=True)
    brand = models.ForeignKey(CameraBrand, on_delete=models.RESTRICT)
    megapixel = models.FloatField(validators=[positive_float])

    def __str__(self):
        return self.name


class DroneBrand(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


def valid_regex(s):
    try:
        re.compile(s)
        return True
    except re.error:
        return False

class DroneModel(models.Model):
    name = models.CharField(max_length=200, unique=True)
    brand = models.ForeignKey(DroneBrand, on_delete=models.RESTRICT)
    supported_cameras = models.ManyToManyField(CameraModel, related_name='supported_drones')
    serial_number_format = models.CharField(max_length=200, validators=[valid_regex])

    def clean(self):
        if not valid_regex(self.serial_number_format):
            raise ValidationError(_('Serial number format must be a valid regular expression'))

    def __str__(self):
        return self.name


class Drone(models.Model):
    name = models.CharField(max_length=200, unique=True)
    serial_number = models.CharField(max_length=200, unique=True)
    drone_model = models.ForeignKey(DroneModel, related_name='drones', on_delete=models.RESTRICT)

    def max_megapixel(self):
        return max([camera.megapixel for camera in self.drone_model.supported_cameras.all()])

    def clean(self):
        if not re.fullmatch(self.model.serial_number_format, self.serial_number):
            raise ValidationError(_('Serial number does not match pattern {} for model {}' \
                .format(self.model.serial_number_format, self.model.name)))

    def __str__(self):
        return self.name
