from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import gettext_lazy as _

from admin_numeric_filter.admin import RangeNumericFilter
from admin_numeric_filter.forms import RangeNumericForm

from .models import Drone, DroneModel, DroneBrand, CameraModel, CameraBrand


class DroneBrandAdmin(admin.ModelAdmin):
    model = DroneBrand
    list_display = ('name',)

class CameraBrandAdmin(admin.ModelAdmin):
    model = CameraBrand
    list_display = ('name',)

class SupportedCamerasAdmin(admin.TabularInline):
    model = DroneModel.supported_cameras.through

class CameraModelAdmin(admin.ModelAdmin):
    model = CameraModel
    list_display = ('name', 'brand', 'megapixel')
    list_filter = ('name', 'brand', ('megapixel', RangeNumericFilter))

class DroneModelAdmin(admin.ModelAdmin):
    model = DroneModel
    inlines = (SupportedCamerasAdmin,)
    list_display = ('name', 'brand', 'serial_number_format')
    list_filter = ('name', 'brand')


# TODO generalize a bit and move into a module
class MaxMegapixelFilter(admin.filters.ListFilter):
    title = 'available megapixel'
    parameter_name = 'available_megapixel'
    template = 'admin/filter_numeric_range.html'

    def __init__(self, request, params, model, model_admin):
        super().__init__(request, params, model, model_admin)
        self.request = request

        if self.parameter_name + '_from' in params:
            value = params.pop(self.parameter_name + '_from')
            self.used_parameters[self.parameter_name + '_from'] = value

        if self.parameter_name + '_to' in params:
            value = params.pop(self.parameter_name + '_to')
            self.used_parameters[self.parameter_name + '_to'] = value

    def has_output(self):
        return True

    def queryset(self, request, queryset):
        value_from = self.used_parameters.get(self.parameter_name + '_from', None)
        value_to = self.used_parameters.get(self.parameter_name + '_to', None)

        filters = {}
        if value_from:
            filters['megapixel__gte'] = value_from
        if value_to:
            filters['megapixel__lte'] = value_to
        return Drone.objects.filter(id__in=CameraModel.objects.filter(**filters).prefetch_related('supported_drones__drones').values('supported_drones__drones').distinct())

    def expected_parameters(self):
        return [
            '{}_from'.format(self.parameter_name),
            '{}_to'.format(self.parameter_name),
        ]

    def choices(self, changelist):
        return ({
            'request': self.request,
            'parameter_name': self.parameter_name,
            'form': RangeNumericForm(name=self.parameter_name, data={
                self.parameter_name + '_from': self.used_parameters.get(self.parameter_name + '_from', None),
                self.parameter_name + '_to': self.used_parameters.get(self.parameter_name + '_to', None),
            }),
        }, )



class DroneAdmin(admin.ModelAdmin):
    model = Drone
    list_display = ('name', 'drone_model', 'serial_number', 'max_megapixel')
    list_filter = ('name', 'drone_model', 'serial_number', MaxMegapixelFilter)



class UserAdminAuthenticationForm(AuthenticationForm):
    error_messages = {
        **AuthenticationForm.error_messages,
        'invalid_login': _(
            "Please enter the correct %(username)s and password for an existing "
            "account. Note that both fields may be case-sensitive."
        ),
    }
    required_css_class = 'required'

    def confirm_login_allowed(self, user):
        super().confirm_login_allowed(user)


class UserAdmin(AdminSite):
    login_form = UserAdminAuthenticationForm

    def has_permission(self, request):
        return request.user.is_active


user_admin_site = UserAdmin(name='usersadmin')


admin_models = [
    (Drone, DroneAdmin),
    (DroneModel, DroneModelAdmin),
    (DroneBrand, DroneBrandAdmin),
    (CameraModel, CameraModelAdmin),
    (CameraBrand, CameraBrandAdmin),
]

for model, model_admin in admin_models:
    admin.site.register(model, model_admin)
    user_admin_site.register(model, model_admin)
